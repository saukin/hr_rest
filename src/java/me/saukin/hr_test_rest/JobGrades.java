/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.saukin.hr_test_rest;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author S_SAUKIN
 */
@Entity
@Table(name = "job_grades")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobGrades.findAll", query = "SELECT j FROM JobGrades j")
    , @NamedQuery(name = "JobGrades.findByGradeLevel", query = "SELECT j FROM JobGrades j WHERE j.gradeLevel = :gradeLevel")
    , @NamedQuery(name = "JobGrades.findByLowestSal", query = "SELECT j FROM JobGrades j WHERE j.lowestSal = :lowestSal")
    , @NamedQuery(name = "JobGrades.findByHighestSal", query = "SELECT j FROM JobGrades j WHERE j.highestSal = :highestSal")})
public class JobGrades implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "grade_level")
    private String gradeLevel;
    @Column(name = "lowest_sal")
    private Integer lowestSal;
    @Column(name = "highest_sal")
    private Integer highestSal;

    public JobGrades() {
    }

    public JobGrades(String gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public String getGradeLevel() {
        return gradeLevel;
    }

    public void setGradeLevel(String gradeLevel) {
        this.gradeLevel = gradeLevel;
    }

    public Integer getLowestSal() {
        return lowestSal;
    }

    public void setLowestSal(Integer lowestSal) {
        this.lowestSal = lowestSal;
    }

    public Integer getHighestSal() {
        return highestSal;
    }

    public void setHighestSal(Integer highestSal) {
        this.highestSal = highestSal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gradeLevel != null ? gradeLevel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobGrades)) {
            return false;
        }
        JobGrades other = (JobGrades) object;
        if ((this.gradeLevel == null && other.gradeLevel != null) || (this.gradeLevel != null && !this.gradeLevel.equals(other.gradeLevel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "me.saukin.hr_test_rest.JobGrades[ gradeLevel=" + gradeLevel + " ]";
    }
    
}
